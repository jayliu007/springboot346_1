package com.iweb.service.impl;

import com.iweb.entity.Pager;
import com.iweb.entity.User;
import com.iweb.entity.UserQuery;
import com.iweb.mapper.UserMapper;
import com.iweb.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public void findPageList(Pager<User> pager) {
        // 查询分页列表
        List<User> userList = userMapper.selectPageList((Map) pager.getQueryParam(), pager.getOffset(), pager.getPageSize());
        pager.setRecordList(userList);
        // 查询分页条数对应数据
        pager.setTotalCount(userMapper.selectPageCount((Map) pager.getQueryParam()));
    }
}
