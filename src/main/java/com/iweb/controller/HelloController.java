package com.iweb.controller;

import com.iweb.entity.ReturnResult;
import com.iweb.sms.service.SmsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author jay
 * @date 2023/4/15
 * @description
 */
@Controller
public class HelloController {
    @Resource
    private SmsService smsService;


    @GetMapping("hello")
    @ResponseBody
    public String hello() {
        return "hello, spring boot";
    }

    @GetMapping("hello2")
    public String hello2() {
        return "hello";
    }

    @GetMapping("hello3")
    @ResponseBody
    public ReturnResult hello3() {
//        int a = 10 / 0;
        return ReturnResult.success(null, "hello3");
    }

    @GetMapping("hello4")
    @ResponseBody
    public ReturnResult hello4() {
        this.smsService.send("1386666888","abcd","eeee","我终于有手机了");
        return  ReturnResult.success(null,"信息发送成功！");
    }
}
