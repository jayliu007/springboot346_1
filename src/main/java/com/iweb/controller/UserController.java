package com.iweb.controller;

import com.iweb.entity.BasePage;
import com.iweb.entity.Pager;
import com.iweb.entity.ReturnResult;
import com.iweb.entity.User;
import com.iweb.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
@RestController
@RequestMapping("user")
@Slf4j
public class UserController {
//    private Logger log = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService userService;

    @GetMapping("list")
    public ReturnResult findList(@RequestBody BasePage page) {
        Pager<User> pager = new Pager<>();
        pager.setPageSize(page.getPageSize());
        pager.setPageIndex(page.getPageIndex());
        pager.setQueryParam(page.getQueryParam());
        userService.findPageList(pager);
        log.debug("进行了一次用户列表的查询{}", page);
        return ReturnResult.success(pager, "查询成功");
    }
}
