package com.iweb.config;

import com.iweb.entity.Boy;
import com.iweb.entity.Girl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
@Configuration
public class MyConfig3 {
    @Bean
    public Girl girl() {
        Girl girl = new Girl();
        return girl;
    }

    @Bean
    // 在不知道bean的id时，可以用类型判断
//    @ConditionalOnBean(Girl.class)
//    @ConditionalOnBean(name={"girl"})
//    @ConditionalOnClass(Girl.class)
    // 通过全限定名判断-不管是否有导入依赖，都可以判断
    @ConditionalOnClass(name = "com.iweb.entity.Girl")
    public Boy boy() {
        Boy boy = new Boy();
        return boy;
    }
}
