package com.iweb.config;

import com.iweb.entity.User;
import com.jay.config.MyConfig2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
@Configuration
// 导入类,,  导入配置类-通常是第三方的
@Import({User.class, MyConfig2.class})
public class MyConfig1 {

}
