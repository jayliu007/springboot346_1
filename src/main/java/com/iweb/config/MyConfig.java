package com.iweb.config;

import com.iweb.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
// 不生成代理对象
@Configuration(proxyBeanMethods = false)
public class MyConfig {
    //    在ioc中生成一个bean，默认用生成bean的方法法作为bean的id
    @Bean
    public User user1() {
        User user = new User();
        return user;
    }

    // 自定义bean的id名
    @Bean("myUser")
    public User user2() {
        User user = new User();
        return user;
    }
}
