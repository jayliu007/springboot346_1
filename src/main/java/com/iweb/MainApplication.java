package com.iweb;

import com.iweb.config.MyConfig;
import com.iweb.entity.Boy;
import com.iweb.entity.Girl;
import com.iweb.entity.Pet;
import com.iweb.entity.User;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author jay
 * @date 2023/4/15
 * @description
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.iweb.mapper"})
public class MainApplication {
    public static void main(String[] args) {
        // 项目启动时 传入控制台参数args,以便灵活配置
        ConfigurableApplicationContext context = SpringApplication.run(MainApplication.class, args);
        String[] definitionNames = context.getBeanDefinitionNames();
        for (String name : definitionNames) {
            System.out.println(name);
        }
        System.out.println("数量：" + definitionNames.length);
//        MyConfig config = context.getBean("myConfig", MyConfig.class);
//        System.out.println(config);
        // proxyBeanMethods = true此处调用的是代理对象的重写的方法
        // proxyBeanMethods = false此处调用的是普通对象的方法
//        User user1 = config.user1();
//        User user2 = config.user1();
//        System.out.println(user1 == user2);

        Girl girl = context.getBean("girl", Girl.class);
        girl.sayHi();
        Boy boy = context.getBean("boy", Boy.class);
        boy.makeFriend();
    }
}
