package com.iweb.mapper;

import com.iweb.entity.User;
import com.iweb.entity.UserQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 分页查询
     *
     * @param params  查询参数对象
     * @param offset 偏移值
     * @param rows   返回行数
     * @return
     */
    List<User> selectPageList(@Param("query") Map<String, Object> params,
                              @Param("offset") Integer offset,
                              @Param("rows") Integer rows);

    int selectPageCount(@Param("query") Map<String, Object> params);
}