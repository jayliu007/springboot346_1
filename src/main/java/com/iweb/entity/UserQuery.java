package com.iweb.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * littlemall_user
 *
 * @author 查询参数实体
 */
@Data
public class UserQuery implements Serializable {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 类型（1：后台 0:前台）
     */
    private Integer type;
    private static final long serialVersionUID = 1L;
}