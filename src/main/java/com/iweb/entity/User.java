package com.iweb.entity;

import java.io.Serializable;
import lombok.Data;
import org.springframework.stereotype.Service;

/**
 * littlemall_user
 * @author 
 */
@Data
public class User implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别(1:男 0：女)
     */
    private Integer sex;

    /**
     * 身份证号
     */
    private String identityCode;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 类型（1：后台 0:前台）
     */
    private Integer type;

    private static final long serialVersionUID = 1L;
}