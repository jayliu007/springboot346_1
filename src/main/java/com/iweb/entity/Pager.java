package com.iweb.entity;

import lombok.Data;

import java.util.List;

/**
 * @author jay
 * @date 2023/3/29
 * @description 分页帮助类
 */
@Data
public class Pager<T> extends BasePage {
    /**
     * 保存查询的数据
     */
    private List<T> recordList;
    /**
     * 总条目数
     */
    private Integer totalCount;
    /**
     * 总页数
     */
    private Integer totalPages;
    /**
     * 分页偏移值
     */
    private Integer offset;



    public Integer getTotalPages() {
        if (totalCount == null || totalCount == 0) {
            totalPages = 0;
        } else {
            totalPages = totalCount % this.getPageSize() == 0 ? totalCount / this.getPageSize() : totalCount / this.getPageSize() + 1;
        }
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getOffset() {
        offset = (this.getPageIndex() - 1) * this.getPageSize();
        return offset;
    }
}
