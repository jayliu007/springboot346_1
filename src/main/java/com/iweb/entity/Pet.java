package com.iweb.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * @author jay
 * @date 2023/4/15
 * @description
 */
@Data
@Component
public class Pet {
    @Value("${pet.name}")
    private String name;
    @Value("${pet.type}")
    private String type;
}
