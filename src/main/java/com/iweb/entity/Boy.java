package com.iweb.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
@Data
public class Boy {
    @Autowired
    private Girl girl;
    private String name;

    public void makeFriend(){
        System.out.println("hello, 小姐姐！");
        girl.sayHi();
    }
}