package com.iweb.entity;

/**
 * @author jay
 * @date 2022/10/10
 * @description 异步请求返回结构体的实体类2.0版本
 */
public class ReturnResult {
    public ReturnResult() {
    }

    private String code;  // 编码：200, 500
    private Object data;  // 数据
    private String message;  // 消息

    public static ReturnResult error() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.code = "500";
        return returnResult;
    }

    public static ReturnResult error(String message) {
        ReturnResult returnResult = ReturnResult.error();
        returnResult.message = message;
        return returnResult;
    }

    public static ReturnResult success() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.code = "200";
        return returnResult;
    }

    public static ReturnResult success(Object data) {
        ReturnResult returnResult = success();
        returnResult.data = data;
        return returnResult;
    }

    public static ReturnResult success(Object data, String message) {
        ReturnResult returnResult = success();
        returnResult.data = data;
        returnResult.message = message;
        return  returnResult;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
