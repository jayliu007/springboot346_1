package com.iweb.entity;

import lombok.Data;

/**
 * @author jay
 * @date 2023/4/17
 * @description
 */
@Data
public class BasePage {
    /**
     * 当前页码
     */
    private Integer pageIndex = 1;
    /**
     * 每页大小
     */
    private Integer pageSize = 5;
    // 查询参数对象
    private Object queryParam;

    public void setPageIndex(Integer pageIndex) {
        if (pageIndex == null) {
            return;
        }
        this.pageIndex = pageIndex;
    }

    public void setPageSize(Integer pageSize) {
        if (pageSize == null) {
            return;
        }
        this.pageSize = pageSize;
    }
}
