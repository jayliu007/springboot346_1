package com.iweb.exception;

import com.iweb.entity.ReturnResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author jay
 * @date 2023/4/17
 * @description 自定义全局异常拦截器
 */
@RestControllerAdvice(basePackages = {"com.iweb.controller"})
public class MyExceptionHandler {

    @ExceptionHandler
    public ReturnResult handleException(Exception e) {
        e.printStackTrace();
        return ReturnResult.error(e.getMessage());
    }
}
