package com.jay.config;

import com.iweb.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jay
 * @date 2023/4/17
 * @description 自定义的，不在启动类子包中的配置类-所谓的第三方
 */
@Configuration
public class MyConfig2 {
    @Bean
    public User user3() {
        User user = new User();
        return user;
    }
}
