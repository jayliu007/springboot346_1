package com.iweb.mapper;

import com.iweb.entity.User;
import com.iweb.entity.UserQuery;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author jay
 * @date 2023/4/15
 * @description
 */
@SpringBootTest
class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    @Test
    void selectByPrimaryKey() {
        User user = userMapper.selectByPrimaryKey(2);
        System.out.println(user);
    }

    @Test
    void selectPageList() {
    }
}